function goodbye(name: string) {
    console.log(`Goodbye ${name}`);
}

function greeting(name: string) {
    console.log(`Hello ${name}`);
}

function main() {
    greeting("Developer");
}

main();
